#!/usr/bin/gnuplot

set title "Thinkpad X201 CPU Temperature"
set xlabel "Minutes"
set ylabel "°C"

set term png
set output "output.png"

plot "data-before.dat" with lines title "Original", \
     "data-after.dat" with lines title "Fan cleaned"

#set term x11
#replot
#pause -1 "Hit any key to continue"
