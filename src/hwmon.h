/*
 * Copyright 2020-2021 Stefan Ott
 *
 * This file is part of warmluft.
 *
 * Warmluft is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HWMON_H
#define HWMON_H

#include <linux/limits.h>

struct hwmon_sensor
{
	char path_in[NAME_MAX + 1];
	char label[128];
	float temp_max;
	struct hwmon_device *device;
	struct hwmon_sensor *next;
};

struct hwmon_device
{
	char path[PATH_MAX];
	char name[128];
	struct hwmon_sensor *sensors;
	struct hwmon_device *next;
};

struct hwmon_device *hwmon_load_devices();
void hwmon_free_devices(struct hwmon_device *);
struct hwmon_sensor *hwmon_dev_by_path(struct hwmon_device *, const char *);
struct hwmon_sensor *hwmon_best_sensor(struct hwmon_device *);

#endif
