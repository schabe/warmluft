About
=====

Warmluft is a program to help testing a computer's cooling system. The program
logs the CPU temperature while generating CPU load.


Building
========

Warmluft uses the meson build system. To compile it, run:

    meson builddir/
    cd builddir/
    ninja


Running tests
=============

You may want to run the unit tests after building:

    ninja test

This should only take a few seconds.


Usage
=====

Warmluft will try to find a suitable temperature sensor. If you are unhappy with
its choice, use the `-d` parameter to specify a different sensor:

    ./warmluft -d /sys/class/hwmon/hwmon0/temp2_input

Usually these sensors define a maximum temperature, warmluft will terminate once
that temperature is reached. If the sensor fails to provide this information,
you will need to do so yourself:

    ./warmluft -m 58

This command will terminate once the sensor reports 58°C. You may also want to
test the temperature on a system that never reaches the maximum:

    ./warmluft -t 20

With this option, the application will terminate after 20 minutes.

When messing with your system's cooling, it's good to know whether you are
making any progress. That's why warmluft can log the current temperature value
to a file at regular (1 minute) intervals:

    ./warmluft -f /path/to/log/file

By default, warmluft will start one child process per CPU core in your system.
These child processes are the ones that generate the system load. If you want a
different number of processes, you can have just that:

    ./warmluft -n 8

This command will start 8 child processes.
